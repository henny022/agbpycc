antlr-4.9.2-complete.jar:
	wget https://www.antlr.org/download/antlr-4.9.2-complete.jar

agbpycc/antlr/ASMLexer.py agbpycc/antlr/ASMParser.py agbpycc/antlr/ASMVisitor.py &: agbpycc/ASM.g4 antlr-4.9.2-complete.jar
	java -jar antlr-4.9.2-complete.jar $< -no-listener -visitor -Dlanguage=Python3 -o agbpycc/antlr -Xexact-output-dir

.PHONY: antlr
antlr: agbpycc/antlr/ASMLexer.py agbpycc/antlr/ASMParser.py agbpycc/antlr/ASMVisitor.py

.PHONY: install
install: antlr
	pip install .

.PHONY: setup
setup: antlr
	pip install -r requirements.txt

.PHONY: lint test
lint:
	pycodestyle test agbpycc --exclude agbpycc/antlr --ignore E501
test:
	python -m pytest test --junit-xml=test_result.xml
