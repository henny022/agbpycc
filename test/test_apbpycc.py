import pytest

from agbpycc.__main__ import main as agbpycc


def run(arguments, expected_exit_code=0, expected_output_filename='', actual_output_filename=''):
    with pytest.raises(SystemExit) as system_exit:
        agbpycc(arguments)
    assert system_exit.value.code == expected_exit_code
    if expected_output_filename:
        with open(expected_output_filename, 'r') as expected_output_file:
            expected_output = expected_output_file.readlines()
        with open(actual_output_filename, 'r') as actual_output_file:
            actual_output = actual_output_file.readlines()
        assert expected_output == actual_output


def test_help():
    run(['--help'])


def test_minimal_c(tmp_path):
    input_file = 'test/test_files/minimal_c.c'
    output_file = 'test/test_files/minimal_c.s'
    actual_file = tmp_path / 'output.s'
    run(
        [
            '-o', str(actual_file),
            '--cc1', 'agbcc',
            input_file
        ],
        expected_output_filename=output_file,
        actual_output_filename=actual_file
    )


def test_minimal_c_O2(tmp_path):
    input_file = 'test/test_files/minimal_c.c'
    output_file = 'test/test_files/minimal_c_O2.s'
    actual_file = tmp_path / 'output.s'
    run(
        [
            '-o', str(actual_file),
            '--cc1', 'agbcc',
            '-O2',
            input_file
        ],
        expected_output_filename=output_file,
        actual_output_filename=actual_file
    )


def test_global_data_O0(tmp_path):
    input_file = 'test/test_files/global_data.c'
    output_file = 'test/test_files/global_data_O0.s'
    actual_file = tmp_path / 'output.s'
    run(
        [
            '-o', str(actual_file),
            '--cc1', 'agbcc',
            '-O0',
            input_file
        ],
        expected_output_filename=output_file,
        actual_output_filename=actual_file
    )


def test_global_data_O1(tmp_path):
    input_file = 'test/test_files/global_data.c'
    output_file = 'test/test_files/global_data_O1.s'
    actual_file = tmp_path / 'output.s'
    run(
        [
            '-o', str(actual_file),
            '--cc1', 'agbcc',
            '-O1',
            input_file
        ],
        expected_output_filename=output_file,
        actual_output_filename=actual_file
    )


def test_global_data_O2(tmp_path):
    input_file = 'test/test_files/global_data.c'
    output_file = 'test/test_files/global_data_O2.s'
    actual_file = tmp_path / 'output.s'
    run(
        [
            '-o', str(actual_file),
            '--cc1', 'agbcc',
            '-O2',
            input_file
        ],
        expected_output_filename=output_file,
        actual_output_filename=actual_file
    )


def test_global_data_g_O0(tmp_path):
    input_file = 'test/test_files/global_data.c'
    output_file = 'test/test_files/global_data_g_O0.s'
    actual_file = tmp_path / 'output.s'
    run(
        [
            '-o', str(actual_file),
            '--cc1', 'agbcc',
            '-O0',
            '-g',
            input_file
        ],
        expected_output_filename=output_file,
        actual_output_filename=actual_file
    )


def test_global_data_g_O1(tmp_path):
    input_file = 'test/test_files/global_data.c'
    output_file = 'test/test_files/global_data_g_O1.s'
    actual_file = tmp_path / 'output.s'
    run(
        [
            '-o', str(actual_file),
            '--cc1', 'agbcc',
            '-O1',
            '-g',
            input_file
        ],
        expected_output_filename=output_file,
        actual_output_filename=actual_file
    )


def test_global_data_g_O2(tmp_path):
    input_file = 'test/test_files/global_data.c'
    output_file = 'test/test_files/global_data_g_O2.s'
    actual_file = tmp_path / 'output.s'
    run(
        [
            '-o', str(actual_file),
            '--cc1', 'agbcc',
            '-O2',
            '-g',
            input_file
        ],
        expected_output_filename=output_file,
        actual_output_filename=actual_file
    )
