import agbpycc.parser


def test_parse_number():
    assert agbpycc.parser.parse_number('1') == 1
    assert agbpycc.parser.parse_number('0x1') == 1
    assert agbpycc.parser.parse_number('0x10') == 16
    assert agbpycc.parser.parse_number('-1') == -1
    assert agbpycc.parser.parse_number('-0x20') == -32
    assert agbpycc.parser.parse_number('0xffffffff') == -1
    assert agbpycc.parser.parse_number('0xffffffd6') == -42
