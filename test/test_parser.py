import itertools
import os
from io import StringIO
from typing import Optional

import pytest

from agbpycc.parser import parse_text, generate_ast, ASTDump, apply_transformations

wrapper_header = """thumb_func_start test_func
test_func:
"""

operation_register_cases = [
    ("r4, r1", "r4, r1"),
    ("r4, r1, r2", "r4, r1, r2"),
    ("r4, r4, r2", "r4, r2"),
]

opreation_instructions = [
    ("adds", "add"),
    ("subs", "sub"),
    ("ands", "and"),
    ("orrs", "orr"),
    ("eors", "eor"),
    ("lsls", "lsl"),
    ("lsrs", "lsr"),
    ("asls", "asl"),
    ("asrs", "asr"),
    ("bics", "bic"),
]

dummy_instruction = "mov r1, r2"


def asm_test_wrapper(input: str, expected_output: str, header: Optional[str] = None):
    header = header if header is not None else wrapper_header
    tree, success = parse_text(header + input)
    assert success, 'could not parse input assembly'
    ast = generate_ast(tree)
    apply_transformations(ast)
    with StringIO() as output:
        ASTDump(output).visit(ast)
        actual_output = output.getvalue().replace(header, "").strip()
        assert actual_output == expected_output.strip()


def asm_test_file(testcase: str):
    current_dir = os.path.dirname(__file__)
    dir_path = os.path.join(current_dir, "test_files/asm_cases", testcase)
    input_path = os.path.join(dir_path, "input.s")
    expected_output_path = os.path.join(dir_path, "output.s")
    with open(input_path, "r") as input, open(expected_output_path, "r") as output:
        asm_test_wrapper(input.read(), output.read(), "")


@pytest.mark.parametrize(
    "operation,expected_operation,parameters,expected_parameters",
    [sum(x, tuple()) for x in itertools.product(opreation_instructions, operation_register_cases)],
)
def test_operation(operation: str, expected_operation: str, parameters: str, expected_parameters: str):
    asm_test_wrapper(f"{operation} {parameters}", f"{expected_operation} {expected_parameters}")


def test_hex_immediate():
    asm_test_wrapper("movs r1, #10", "mov r1, #0xa")


def test_movs():
    asm_test_wrapper("movs r1, r2", "mov r1, r2")


def test_mov():
    asm_test_wrapper("mov r1, r2", "mov r1, r2")


def test_mvns():
    asm_test_wrapper("mvns r1, r2", "mvn r1, r2")


def test_mvn():
    asm_test_wrapper("mvn r1, r2", "mvn r1, r2")


def test_adds_as_mov():
    asm_test_wrapper("adds r1, r2, #0", "mov r1, r2")


def test_adds_negative():
    asm_test_wrapper("adds r1, r2, #-3", "sub r1, r2, #0x3")


def test_subs_negative():
    asm_test_wrapper("subs r1, r2, #-3", "add r1, r2, #0x3")


def test_pop():
    asm_test_wrapper("pop {r1}", "pop {r1}")


def test_pop_multiple():
    asm_test_wrapper("pop {r1, r2}", "pop {r1, r2}")


def test_push():
    asm_test_wrapper("push {r1}", "push {r1}")


def test_push_multiple():
    asm_test_wrapper("push {r1, r2}", "push {r1, r2}")


def test_rsbs():
    asm_test_wrapper("rsbs r1, r2, #0", "neg r1, r2")


def test_rsb():
    asm_test_wrapper("rsb r1, r2, #0", "neg r1, r2")


def test_rsb_non_zero_fails():
    with pytest.raises(ValueError):
        asm_test_wrapper("rsb r1, r2, #3", "")


def test_rsbs_self():
    asm_test_wrapper("rsbs r1, r1, #0", "neg r1, r1")


def test_neg():
    asm_test_wrapper("neg r1, r2", "neg r1, r2")


def test_muls_1_register():
    asm_test_wrapper("muls r1, r2", "mul r1, r2")


def test_muls():
    asm_test_wrapper("muls r1, r1, r2", "mul r1, r2")


def test_muls_2nd_register():
    asm_test_wrapper("muls r1, r2, r1", "mul r1, r2")


def test_mul():
    asm_test_wrapper("mul r1, r1, r2", "mul r1, r2")


@pytest.mark.parametrize(
    "instruction,source",
    itertools.product(
        ("ldr", "ldrh", "ldrsh", "ldrb", "ldrsb"),
        ("r1, r2", "r1, #0x4", "r4"),
    ),
)
def test_ldr(instruction: str, source: str):
    asm_test_wrapper(f"{instruction} r1, [{source}]", f"{instruction} r1, [{source}]")


def test_ldr_pc():
    asm_test_wrapper(
        "ldr r1, data1\ndata1: .4byte gValue",
        "ldr r1, _data0_0 @ gValue\n_data0_0:\n\t.4byte gValue"
    )


@pytest.mark.parametrize(
    "instruction,source",
    itertools.product(
        ("str", "strh", "strb"),
        ("r1, r2", "r1, #0x4", "r4"),
    ),
)
def test_str(instruction: str, source: str):
    asm_test_wrapper(f"{instruction} r1, [{source}]", f"{instruction} r1, [{source}]")


def test_ldm():
    asm_test_wrapper("ldm r1!, {r2, r3}", "ldm r1!, {r2, r3}")


def test_ldmia():
    asm_test_wrapper("ldmia r1!, {r2, r3}", "ldm r1!, {r2, r3}")


def test_stm():
    asm_test_wrapper("stm r1!, {r2, r3}", "stm r1!, {r2, r3}")


def test_stmia():
    asm_test_wrapper("stmia r1!, {r2, r3}", "stm r1!, {r2, r3}")


@pytest.mark.parametrize(
    "instruction,regs",
    itertools.product(
        ("cmp", "cmn"),
        ("r1, r2", "r1, #0x4"),
    ),
)
def test_condition(instruction: str, regs: str):
    asm_test_wrapper(f"{instruction} {regs}", f"{instruction} {regs}")


def _test_branch(instruction: str, expected_instruction: str):
    asm_test_wrapper(f"{instruction} _0802E2F0\n_0802E2F0:", f"{expected_instruction} _code0_0\n_code0_0:")


@pytest.mark.parametrize(
    "instruction",
    ("b", "beq", "bne", "bhs", "blo", "bmi", "bpl", "bvs", "bvc",
     "bhi", "bls", "bge", "blt", "bgt", "ble")
)
def test_branch(instruction: str):
    _test_branch(instruction, instruction)


def test_bcc():
    _test_branch("bcc", "blo")


def test_bcs():
    _test_branch("bcs", "bhs")


def test_bl():
    asm_test_wrapper("bl function_name", "bl function_name")


def test_bx():
    asm_test_wrapper("bx r1", "bx r1")


def test_multiple_code_labels():
    asm_test_wrapper(
        f"b .l1\n"
        f"b .l2\n"
        f"b .l3\n"
        f".l1:\n"
        f"{dummy_instruction}\n"
        f".l2:\n"
        f"{dummy_instruction}\n"
        f".l3:\n"
        f"{dummy_instruction}\n",
        f"b _code0_0\n"
        f"\tb _code0_1\n"
        f"\tb _code0_2\n"
        f"_code0_0:\n"
        f"\t{dummy_instruction}\n"
        f"_code0_1:\n"
        f"\t{dummy_instruction}\n"
        f"_code0_2:\n"
        f"\t{dummy_instruction}")


def test_collapse_code_labels():
    asm_test_wrapper(
        f"b .l1\n"
        f"b .l2\n"
        f"b .l3\n"
        f".l1:\n"
        f".l2:\n"
        f".l3:\n"
        f"{dummy_instruction}\n",
        f"b _code0_0\n"
        f"\tb _code0_0\n"
        f"\tb _code0_0\n"
        f"_code0_0:\n"
        f"\t{dummy_instruction}")


def test_comment_removed():
    asm_test_wrapper(f"{dummy_instruction} @should be removed", f"{dummy_instruction}")


def test_switch_cases():
    asm_test_file("switch")


@pytest.mark.parametrize(
    "register,expected_register",
    [
        ("sb", "r9"),
        ("sl", "r10"),
        ("ip", "r12"),
        ("sp", "sp"),
        ("lr", "lr"),
        ("pc", "pc"),
    ],
)
def test_non_number_register(register: str, expected_register: str):
    asm_test_wrapper(f"mov r1, {register}", f"mov r1, {expected_register}")
