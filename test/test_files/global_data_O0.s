
i::
	.4byte 0x1

data::
	.1byte 0x1
	.1byte 0x2
	.1byte 0x3
	.1byte 0x4

	thumb_func_start test
test:
	push {r7, lr}
	mov r7, sp
	mov r0, #0x4
	b _code0_0
_code0_0:
	pop {r7, pc}
_other0_0:
_data0_0:
	.4byte i

	thumb_func_start main
main:
	push {r7, lr}
	mov r7, sp
	bl __gccmain
	ldr r0, _data1_0 @ i
	ldr r1, [r0]
	mov r0, r1
	b _code1_0
_other1_0:
_data1_0:
	.4byte i
_code1_0:
	pop {r7, pc}
_other1_1:
