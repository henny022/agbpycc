thumb_func_start test_func
test_func:
	lsl r0, #0x2
	ldr r1, _data0_0 @ _switch0_0
	add r0, r1
	ldr r0, [r0]
	mov pc, r0
_data0_0:
	.4byte _switch0_0
_switch0_0:
	.4byte _case0_0_0	@ case 0
	.4byte _case0_0_1	@ case 1
_case0_0_0:
	mov r1, r2
_case0_0_1:
	mov r2, r3
