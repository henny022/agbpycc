thumb_func_start test_func
test_func:
    lsls r0, r0, #2
	ldr r1, switch_table_addr
	adds r0, r0, r1
	ldr r0, [r0]
	mov pc, r0
	.align 2, 0
switch_table_addr: .4byte switch_table
switch_table:
	.4byte case0
	.4byte case1
case0:
    mov r1, r2
case1:
    mov r2, r3
