import logging
from typing import TextIO, Dict

import antlr4

from .antlr import ASMLexer, ASMParser, ASMVisitor
from .ast import *


def parse_file(filename: str) -> (ASMParser.AsmfileContext, bool):
    return parse(antlr4.FileStream(filename))


def parse_text(text: str) -> (ASMParser.AsmfileContext, bool):
    return parse(antlr4.InputStream(text))


def parse(input_stream: antlr4.InputStream) -> (ASMParser.AsmfileContext, bool):
    lexer = ASMLexer(input_stream)
    parser = ASMParser(antlr4.CommonTokenStream(lexer))
    tree = parser.asmfile()
    return tree, parser.getNumberOfSyntaxErrors() == 0


def parse_number(text: str) -> int:
    i = int(text, 0)
    if i < 0:
        i += 1 << 32
    if i >= 0x80000000:
        i -= 1 << 32
    return i


class ASTGenerator(ASMVisitor):
    def visitReg(self, ctx: ASMParser.RegContext):
        return Register(ctx.REG().symbol.text)

    def visitImm(self, ctx: ASMParser.ImmContext):
        return Constant(ctx.NUM().symbol.text)

    def visitReglist(self, ctx: ASMParser.ReglistContext):
        registers = []
        for register in ctx.reg():
            registers.append(self.visit(register))
        return registers

    def visitAsmfile(self, ctx: ASMParser.AsmfileContext):
        tlcs = []
        for tlc_ctx in ctx.tlc():
            tlc = self.visit(tlc_ctx)
            if tlc is not None:
                tlcs.append(tlc)
            else:
                logging.debug(f'no rule to process input {tlc_ctx.getText()}')
        return ASMFile(tlcs)

    def visitTlc(self, ctx: ASMParser.TlcContext):
        return self.visit(ctx.children[0])

    def visitGlobal_data(self, ctx: ASMParser.Global_dataContext):
        name = self.visit(ctx.global_data_header())
        lines = []
        for line in ctx.line():
            linep = self.visit(line)
            if linep is not None:
                lines.append(linep)
            else:
                logging.debug(f'no rule to process input {line.getText()}')
        return GlobalData(name, lines)

    def visitGlobal_data_header_small(self, ctx: ASMParser.Global_data_header_smallContext):
        return None

    def visitGlobal_data_header_large(self, ctx: ASMParser.Global_data_header_largeContext):
        return self.visit(ctx.globl())

    def visitGlobl(self, ctx: ASMParser.GloblContext):
        return ctx.name.text

    def visitFunction(self, ctx: ASMParser.FunctionContext):
        name = self.visit(ctx.function_header())
        instructions = []
        for line in ctx.line():
            linep = self.visit(line)
            if linep is not None:
                if not isinstance(linep, Instruction):
                    print(f'bad line {line.getText()}')
                instructions.append(linep)
            else:
                logging.debug(f'no rule to process input {line.getText()}')
        return Function(name, instructions)

    def visitFunction_header1(self, ctx: ASMParser.Function_header1Context):
        return ctx.name.text

    def visitFunction_header2(self, ctx: ASMParser.Function_header2Context):
        return self.visit(ctx.globl())

    def visitPush_multiple(self, ctx: ASMParser.Push_multipleContext):
        registers = self.visit(ctx.reglist())
        return PUSH(registers)

    def visitPop_multiple(self, ctx: ASMParser.Pop_multipleContext):
        registers = self.visit(ctx.reglist())
        return POP(registers)

    def visitLabel(self, ctx: ASMParser.LabelContext):
        return LABEL(ctx.name.text)

    def visitData1word(self, ctx: ASMParser.Data1wordContext):
        return DATA(1, ctx.const.text)

    def visitData2word(self, ctx: ASMParser.Data2wordContext):
        return DATA(2, ctx.const.text)

    def visitData4word(self, ctx: ASMParser.Data4wordContext):
        if ctx.offset:
            offset = int(ctx.offset.text, 0)
        else:
            offset = None
        return DATA(4, ctx.const.text, offset)

    def visitData1num(self, ctx: ASMParser.Data1numContext):
        return DATA(1, parse_number(ctx.const.text))

    def visitData2num(self, ctx: ASMParser.Data2numContext):
        return DATA(2, parse_number(ctx.const.text))

    def visitData4num(self, ctx: ASMParser.Data4numContext):
        return DATA(4, parse_number(ctx.const.text))

    def operation(self, ctx, cls):
        rd = self.visit(ctx.rd)
        rn = self.visit(ctx.rn) if ctx.rn else rd
        rm = self.visit(ctx.rm)
        return cls(rd, rn, rm)

    def visitAdd(self, ctx: ASMParser.AddContext):
        return self.operation(ctx, ADD)

    def visitSub(self, ctx: ASMParser.SubContext):
        return self.operation(ctx, SUB)

    def visitRsb(self, ctx: ASMParser.RsbContext):
        imm = self.visit(ctx.imm())
        if not imm.value == 0:
            raise ValueError('rsb only allowed with 0 immediate')
        rd = self.visit(ctx.rd)
        rn = self.visit(ctx.rn)
        return NEG(rd, rn)

    def visitNeg(self, ctx: ASMParser.NegContext):
        rd = self.visit(ctx.rd)
        rm = self.visit(ctx.rm)
        return NEG(rd, rm)

    def visitMul1(self, ctx: ASMParser.Mul1Context):
        rd = self.visit(ctx.rd)
        rn = self.visit(ctx.rn)
        return MUL(rd, rn, rd)

    def visitMul2(self, ctx: ASMParser.Mul2Context):
        rd = self.visit(ctx.rd)
        rn = self.visit(ctx.rn)
        rm = self.visit(ctx.rm)
        return MUL(rd, rn, rm)

    def visitLand(self, ctx: ASMParser.LandContext):
        return self.operation(ctx, AND)

    def visitOrr(self, ctx: ASMParser.OrrContext):
        return self.operation(ctx, ORR)

    def visitEor(self, ctx: ASMParser.EorContext):
        return self.operation(ctx, EOR)

    def visitLsl(self, ctx: ASMParser.LslContext):
        return self.operation(ctx, LSL)

    def visitLsr(self, ctx: ASMParser.LsrContext):
        return self.operation(ctx, LSR)

    def visitAsl(self, ctx: ASMParser.AslContext):
        return self.operation(ctx, ASL)

    def visitAsr(self, ctx: ASMParser.AsrContext):
        return self.operation(ctx, ASR)

    def visitBic(self, ctx: ASMParser.BicContext):
        return self.operation(ctx, BIC)

    def visitLdr_pc(self, ctx: ASMParser.Ldr_pcContext):
        rt = self.visit(ctx.rt)
        label = ctx.target.text
        if ctx.offset:
            offset = int(ctx.offset.text, 0)
            return LDR_PC(rt, label, offset)
        return LDR_PC(rt, label)

    def ldr(self, ctx, size: int = 4, signed: bool = False):
        rt = self.visit(ctx.rt)
        rn = self.visit(ctx.rn)
        rm = self.visit(ctx.rm) if ctx.rm else None
        return LDR(rt, rn, rm, size, signed)

    def visitLdr_offset(self, ctx: ASMParser.Ldr_offsetContext):
        return self.ldr(ctx)

    def visitLdrh_offset(self, ctx: ASMParser.Ldrh_offsetContext):
        return self.ldr(ctx, 2, False)

    def visitLdrsh_offset(self, ctx: ASMParser.Ldrsh_offsetContext):
        return self.ldr(ctx, 2, True)

    def visitLdrb_offset(self, ctx: ASMParser.Ldrb_offsetContext):
        return self.ldr(ctx, 1, False)

    def visitLdrsb_offset(self, ctx: ASMParser.Ldrsb_offsetContext):
        return self.ldr(ctx, 1, True)

    def str(self, ctx, size: int = 4):
        rt = self.visit(ctx.rt)
        rn = self.visit(ctx.rn)
        rm = self.visit(ctx.rm) if ctx.rm else None
        return STR(rt, rn, rm, size)

    def visitStr_offset(self, ctx: ASMParser.Str_offsetContext):
        return self.str(ctx)

    def visitStrh_offset(self, ctx: ASMParser.Strh_offsetContext):
        return self.str(ctx, 2)

    def visitStrb_offset(self, ctx: ASMParser.Strb_offsetContext):
        return self.str(ctx, 1)

    def visitStm(self, ctx: ASMParser.StmContext):
        rn = self.visit(ctx.rn)
        registers = self.visit(ctx.reglist())
        return STM(rn, registers)

    def visitLdm(self, ctx: ASMParser.LdmContext):
        rn = self.visit(ctx.rn)
        registers = self.visit(ctx.reglist())
        return LDM(rn, registers)

    def visitBl(self, ctx: ASMParser.BlContext):
        return BL(ctx.target.text)

    def visitBx(self, ctx: ASMParser.BxContext):
        return BX(self.visit(ctx.rm))

    def visitB(self, ctx: ASMParser.BContext):
        return B(ctx.target.text)

    def visitBeq(self, ctx: ASMParser.BeqContext):
        return BEQ(ctx.target.text)

    def visitBne(self, ctx: ASMParser.BneContext):
        return BNE(ctx.target.text)

    def visitBhs(self, ctx: ASMParser.BhsContext):
        return BHS(ctx.target.text)

    def visitBlo(self, ctx: ASMParser.BloContext):
        return BLO(ctx.target.text)

    def visitBmi(self, ctx: ASMParser.BmiContext):
        return BMI(ctx.target.text)

    def visitBpl(self, ctx: ASMParser.BplContext):
        return BPL(ctx.target.text)

    def visitBvs(self, ctx: ASMParser.BvsContext):
        return BVS(ctx.target.text)

    def visitBvc(self, ctx: ASMParser.BvcContext):
        return BVC(ctx.target.text)

    def visitBhi(self, ctx: ASMParser.BhiContext):
        return BHI(ctx.target.text)

    def visitBls(self, ctx: ASMParser.BlsContext):
        return BLS(ctx.target.text)

    def visitBge(self, ctx: ASMParser.BgeContext):
        return BGE(ctx.target.text)

    def visitBlt(self, ctx: ASMParser.BltContext):
        return BLT(ctx.target.text)

    def visitBgt(self, ctx: ASMParser.BgtContext):
        return BGT(ctx.target.text)

    def visitBle(self, ctx: ASMParser.BleContext):
        return BLE(ctx.target.text)

    def visitCmp(self, ctx: ASMParser.CmpContext):
        rn = self.visit(ctx.rn)
        rm = self.visit(ctx.rm)
        return CMP(rn, rm)

    def visitCmn(self, ctx: ASMParser.CmnContext):
        rn = self.visit(ctx.rn)
        rm = self.visit(ctx.rm)
        return CMN(rn, rm)

    def visitMov(self, ctx: ASMParser.MovContext):
        rd = self.visit(ctx.rd)
        rm = self.visit(ctx.rm)
        return MOV(rd, rm)

    def visitMvn(self, ctx: ASMParser.MovContext):
        rd = self.visit(ctx.rd)
        rm = self.visit(ctx.rm)
        return MVN(rd, rm)

    def visitAlign(self, ctx: ASMParser.AlignContext):
        alignment = int(ctx.alignment.text)
        fill = int(ctx.fill.text)
        return Directive(f'.align {alignment}, {fill}')

    def visitSyntax(self, ctx: ASMParser.SyntaxContext):
        # return Directive(f'.syntax {ctx.s.text}')
        return None

    def visitSection_general(self, ctx: ASMParser.Section_generalContext):
        return Section(ctx.s.text)

    def visitSection_text(self, ctx: ASMParser.Section_textContext):
        return Section('.text')

    def visitDir_code(self, ctx: ASMParser.Dir_codeContext):
        return None

    def visitDir_size(self, ctx: ASMParser.Dir_sizeContext):
        return None

    def visitDir_file(self, ctx: ASMParser.Dir_fileContext):
        id = int(ctx.file_id.text, 0)
        name = ctx.file_path.text.strip('"')
        return FileDirective(id, name)

    def visitDir_loc(self, ctx: ASMParser.Dir_locContext):
        file = int(ctx.file_id.text, 0)
        line = int(ctx.file_line.text, 0)
        column = int(ctx.file_column.text, 0)
        return LocDirective(file, line, column)


def push_loc_after_label(asmfile: ASMFile):
    for function in asmfile.functions:
        for i, instruction in enumerate(function.instructions):
            if isinstance(instruction, LocDirective):
                try:
                    if isinstance(function.instructions[i + 1], LABEL):
                        t = function.instructions[i]
                        function.instructions[i] = function.instructions[i + 1]
                        function.instructions[i + 1] = t
                except IndexError:
                    pass


def link_instructions(asmfile: ASMFile):
    for function in asmfile.functions:
        prev_insn: Optional[Instruction] = None
        for instruction in function.instructions:
            if prev_insn is not None:
                instruction._prev = ref(prev_insn)
                prev_insn._next = ref(instruction)
            prev_insn = instruction
            if isinstance(instruction, Branch):
                for label in function.instructions:
                    if isinstance(label, LABEL):
                        if label.name == instruction.label:
                            instruction._target = ref(label)
            if isinstance(instruction, LDR_PC):
                for label in function.instructions:
                    if isinstance(label, LABEL):
                        if label.name == instruction.label:
                            instruction._target = ref(label)
                            label.loads.append(instruction)
            if isinstance(instruction, DATA):
                for label in function.instructions:
                    if isinstance(label, LABEL):
                        if label.name == instruction.data:
                            instruction._target = ref(label)


class ASTVisitor:
    def visit(self, node: ASTNode):
        name = type(node).__name__
        return getattr(self, f'visit_{name.lower()}')(node)

    def visit_asmfile(self, asmfile: ASMFile):
        ret = []
        for tlc in asmfile.tlcs:
            ret.append(self.visit(tlc))
        return ret

    def visit_globaldata(self, global_data: GlobalData):
        ret = []
        for line in global_data.lines:
            ret.append(self.visit(line))
        return ret

    def visit_function(self, function: Function):
        ret = []
        for instruction in function.instructions:
            ret.append(self.visit(instruction))
        return ret

    def instruction(self, instruction: Instruction):
        pass

    def operation(self, operation: Operation):
        return self.instruction(operation)

    def branch(self, branch: Branch):
        return self.instruction(branch)

    def visit_label(self, label: LABEL):
        return self.instruction(label)

    def visit_data(self, data: DATA):
        return self.instruction(data)

    def visit_push(self, push: PUSH):
        return self.instruction(push)

    def visit_pop(self, pop: POP):
        return self.instruction(pop)

    def visit_add(self, add: ADD):
        return self.operation(add)

    def visit_sub(self, sub: SUB):
        return self.operation(sub)

    def visit_neg(self, neg: NEG):
        return self.instruction(neg)

    def visit_mul(self, mul: MUL):
        return self.instruction(mul)

    def visit_and(self, land: AND):
        return self.operation(land)

    def visit_orr(self, orr: ORR):
        return self.operation(orr)

    def visit_eor(self, eor: EOR):
        return self.operation(eor)

    def visit_lsl(self, lsl: LSL):
        return self.operation(lsl)

    def visit_lsr(self, lsr: LSR):
        return self.operation(lsr)

    def visit_asl(self, asl: ASL):
        return self.operation(asl)

    def visit_asr(self, asr: ASR):
        return self.operation(asr)

    def visit_bic(self, bic: BIC):
        return self.operation(bic)

    def visit_ldr_pc(self, ldr_pc: LDR_PC):
        return self.instruction(ldr_pc)

    def visit_ldr(self, ldr: LDR):
        return self.instruction(ldr)

    def visit_str(self, store: STR):
        return self.instruction(store)

    def visit_stm(self, stm: STM):
        return self.instruction(stm)

    def visit_ldm(self, ldm: LDM):
        return self.instruction(ldm)

    def visit_bl(self, bl: BL):
        return self.instruction(bl)

    def visit_bx(self, bx: BX):
        return self.instruction(bx)

    def visit_b(self, b: B):
        return self.branch(b)

    def visit_beq(self, beq: BEQ):
        return self.branch(beq)

    def visit_bne(self, bne: BNE):
        return self.branch(bne)

    def visit_bhs(self, bhs: BHS):
        return self.branch(bhs)

    def visit_blo(self, blo: BLO):
        return self.branch(blo)

    def visit_bmi(self, bmi: BMI):
        return self.branch(bmi)

    def visit_bpl(self, bpl: BPL):
        return self.branch(bpl)

    def visit_bvs(self, bvs: BVS):
        return self.branch(bvs)

    def visit_bvc(self, bvc: BVC):
        return self.branch(bvc)

    def visit_bhi(self, bhi: BHI):
        return self.branch(bhi)

    def visit_bls(self, bls: BLS):
        return self.branch(bls)

    def visit_bge(self, bge: BGE):
        return self.branch(bge)

    def visit_blt(self, blt: BLT):
        return self.branch(blt)

    def visit_bgt(self, bgt: BGT):
        return self.branch(bgt)

    def visit_ble(self, ble: BLE):
        return self.branch(ble)

    def visit_cmp(self, cmp: CMP):
        return self.instruction(cmp)

    def visit_cmn(self, cmn: CMN):
        return self.instruction(cmn)

    def visit_mov(self, mov: MOV):
        return self.instruction(mov)

    def visit_mvn(self, mvn: MVN):
        return self.instruction(mvn)

    def visit_directive(self, directive: Directive):
        return self.instruction(directive)

    def visit_filedirective(self, file: FileDirective):
        return self.visit_directive(file)

    def visit_locdirective(self, loc: LocDirective):
        return self.visit_directive(loc)

    def visit_section(self, section: Section):
        return self.visit_directive(section)


class FindReplacementLabels(ASTVisitor):
    label_replacements: Dict[LABEL, LABEL]

    def __init__(self):
        self.label_replacements = {}

    def visit_label(self, label: LABEL):
        if label.prev and isinstance(label.prev, LABEL):
            if label.prev in self.label_replacements:
                replacement = self.label_replacements[label.prev]
            else:
                replacement = label.prev
            self.label_replacements[label] = replacement
            replacement.loads.extend(label.loads)


class ReplaceLabels(ASTVisitor):
    label_replacements: Dict[LABEL, LABEL]

    def __init__(self, label_replacements):
        self.label_replacements = label_replacements

    def _replace(self, instruction):
        if instruction.target and instruction.target in self.label_replacements:
            instruction._target = ref(self.label_replacements[instruction.target])

    def visit_data(self, data: DATA):
        self._replace(data)

    def visit_ldr_pc(self, ldr_pc: LDR_PC):
        self._replace(ldr_pc)

    def branch(self, branch: Branch):
        self._replace(branch)


class CollapseLabels:
    def visit(self, asmfile: ASMFile):
        # find labels to join
        finder = FindReplacementLabels()
        finder.visit(asmfile)
        replacements = finder.label_replacements
        # replace label
        ReplaceLabels(replacements).visit(asmfile)
        # remove old labels
        for function in asmfile.functions:
            instructions = []
            for instruction in function.instructions:
                if instruction in replacements:
                    # relink instruction list
                    if instruction.prev:
                        instruction._prev = instruction._next
                    if instruction.next:
                        instruction._next = instruction._prev
                else:
                    instructions.append(instruction)
            function.instructions = instructions


class CollectLabels(ASTVisitor):
    current_function: Function

    def visit_function(self, function: Function):
        self.current_function = function
        function.labels = []
        super(CollectLabels, self).visit_function(function)

    def visit_label(self, label: LABEL):
        self.current_function.labels.append(label)


class ClassifyLabels(ASTVisitor):
    current_function: Function

    def visit_function(self, function: Function):
        self.current_function = function
        super(ClassifyLabels, self).visit_function(function)

    def visit_label(self, label: LABEL):
        if isinstance(label.next, DATA):
            label.type = LabelType.DATA

    def branch(self, branch: Branch):
        branch.target.type = LabelType.CODE

    def visit_data(self, data: DATA):
        if data.target:
            data.target.type = LabelType.CASE


class ProcessSwitchTable(ASTVisitor):
    def visit_data(self, data: DATA):
        label = data.target
        if label:
            if isinstance(label.next, DATA):
                if label.next.target.type in [LabelType.CODE, LabelType.CASE]:
                    label.type = LabelType.SWITCH
                    case = label.next
                    ncases = 0
                    while isinstance(case, DATA) \
                            and case.target and \
                            case.target.type in [LabelType.CODE, LabelType.CASE]:
                        case.target.type = LabelType.CASE
                        case.target.case = ncases
                        ncases += 1
                        case = case.next


class RenameLabels(ASTVisitor):
    ncode: int
    ndata: int
    nswitch: int
    nother: int
    nerror: int
    nfunction: int = 0

    def visit_function(self, function: Function):
        self.ncode = 0
        self.ndata = 0
        self.nswitch = -1
        self.nother = 0
        self.nerror = 0
        super(RenameLabels, self).visit_function(function)
        self.nfunction += 1

    def visit_label(self, label: LABEL):
        if label.type == LabelType.CODE:
            label.name = f'_code{self.nfunction}_{self.ncode}'
            self.ncode += 1
        if label.type == LabelType.CASE:
            if label.case is None:
                label.name = f'_LABEL_I_AM_AN_ERROR_PLEASE_REPORT_ME_{self.nerror}'
            else:
                label.name = f'_case{self.nfunction}_{self.nswitch}_{label.case}'
        if label.type == LabelType.DATA:
            label.name = f'_data{self.nfunction}_{self.ndata}'
            self.ndata += 1
        if label.type == LabelType.SWITCH:
            self.nswitch += 1
            label.name = f'_switch{self.nfunction}_{self.nswitch}'
        if label.type == LabelType.OTHER:
            label.name = f'_other{self.nfunction}_{self.nother}'
            self.nother += 1


def merge_data_labels(ast: ASMFile):
    for function in ast.functions:
        instruction = function.instructions[0]
        current_data: Optional[LABEL] = None
        ndata = 0
        while instruction is not None:
            if isinstance(instruction, LABEL):
                if instruction.type == LabelType.DATA:
                    if current_data:
                        for load in instruction.loads:
                            load._target = ref(current_data)
                            load.offset += ndata * 4
                        instruction.type = LabelType.OTHER
                    else:
                        current_data = instruction
            elif isinstance(instruction, DATA):
                ndata += 1
            else:
                current_data = None
                ndata = 0
            instruction = instruction.next


class PatchInstructions(ASTVisitor):
    def visit_function(self, function: Function):
        news = []
        prev: Optional[Instruction] = None
        for i, instruction in enumerate(function.instructions):
            new: Optional[Instruction] = self.visit(instruction)
            if new:
                if prev:
                    prev._next = ref(new)
                    new._prev = ref(prev)
                else:
                    new._prev = None
                prev = new
                news.append(new)
        if prev:
            prev._next = None
        function.instructions = news

    def instruction(self, instruction: Instruction):
        return instruction

    def visit_add(self, add: ADD):
        if isinstance(add.rm, Constant):
            if add.rm.value == 0:
                return MOV(add.rd, add.rn)
            elif add.rm.value < 0:
                return SUB(add.rd, add.rn, Constant(-add.rm.value))
        return add

    def visit_sub(self, sub: SUB):
        if isinstance(sub.rm, Constant):
            if sub.rm.value < 0:
                return ADD(sub.rd, sub.rn, Constant(-sub.rm.value))
        return sub

    def visit_directive(self, directive: Directive):
        return None

    def visit_filedirective(self, file: FileDirective):
        return file

    def visit_locdirective(self, loc: LocDirective):
        return loc


def apply_transformations(ast: ASMFile):
    PatchInstructions().visit(ast)
    merge_data_labels(ast)
    ProcessSwitchTable().visit(ast)
    RenameLabels().visit(ast)


class ASTDump(ASTVisitor):
    file: TextIO

    def __init__(self, file: TextIO):
        self.file = file

    def visit_function(self, function: Function):
        self.file.write(f'\n\tthumb_func_start {function.name}\n{function.name}:\n')
        super(ASTDump, self).visit_function(function)

    def visit_globaldata(self, global_data: GlobalData):
        if not global_data.anon:
            self.file.write(f'\n{global_data.name}::\n')
            super(ASTDump, self).visit_globaldata(global_data)

    def visit_label(self, label: LABEL):
        self.file.write(f'{label.name}:\n')

    def instruction(self, instruction: Instruction):
        self.file.write(f'\t{instruction}\n')

    def visit_section(self, section: Section):
        pass


def generate_ast(tree: ASMParser.AsmfileContext) -> ASMFile:
    ast = ASTGenerator().visit(tree)
    push_loc_after_label(ast)
    link_instructions(ast)
    CollapseLabels().visit(ast)
    CollectLabels().visit(ast)
    ClassifyLabels().visit(ast)
    return ast
