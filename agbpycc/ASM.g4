grammar ASM;

asmfile: tlc+ EOF;
tlc: function | global_data | global_directive;
function: section? function_header line+;
function_header: function_header1 | function_header2;
function_header1: 'thumb_func_start' name=WORD label;
function_header2: align	globl dir_type '.thumb_func' label;

global_data: global_data_header line+ dir_size?;
global_data_header: global_data_header_large | global_data_header_small;
global_data_header_large: globl section? align? dir_type dir_size? label;
global_data_header_small: section? align? label;

line: (instruction | directive | label);
label: name=WORD ':';
instruction: push | pop | arithmetic | logic | lsl | lsr | asl | asr | bic | mov | mvn | branch | ldr | ldm | store | stm | cmp | cmn;

push: push_multiple;
push_multiple: PUSH reglist;

pop: pop_multiple;
pop_multiple: POP reglist;

arithmetic: add | sub | mul | rsb | neg;

add: ADD rd=reg (COMMA rn=reg)? COMMA rm=regimm;
sub: SUB rd=reg (COMMA rn=reg)? COMMA rm=regimm;

mul: mul1 | mul2;
mul1: MUL rd=reg COMMA rn=reg;
mul2: MUL rd=reg COMMA rn=reg COMMA rm=reg;

rsb: RSB rd=reg COMMA rn=reg COMMA imm;
neg: NEG rd=reg COMMA rm=reg;

logic: land | orr | eor;
land: AND rd=reg (COMMA rn=reg)? COMMA rm=regimm;
orr: ORR rd=reg (COMMA rn=reg)? COMMA rm=regimm;
eor: EOR rd=reg (COMMA rn=reg)? COMMA rm=regimm;

lsl: LSL rd=reg (COMMA rn=reg)? COMMA rm=regimm;
lsr: LSR rd=reg (COMMA rn=reg)? COMMA rm=regimm;
asl: ASL rd=reg (COMMA rn=reg)? COMMA rm=regimm;
asr: ASR rd=reg (COMMA rn=reg)? COMMA rm=regimm;

bic: BIC rd=reg (COMMA rn=reg)? COMMA rm=regimm;

mov: MOV rd=reg COMMA rm=regimm;
mvn: MVN rd=reg COMMA rm=reg;

branch: b | bl | bx | beq | bne | bhs | blo | bmi | bpl | bvs | bvc | bhi | bls | bge | blt | bgt | ble;
b: B target=WORD;
bl: BL target=WORD;
bx: BX rm=reg;
beq: BEQ target=WORD;
bne: BNE target=WORD;
bhs: (BCS | BHS) target=WORD;
blo: (BCC | BLO) target=WORD;
bmi: BMI target=WORD;
bpl: BPL target=WORD;
bvs: BVS target=WORD;
bvc: BVC target=WORD;
bhi: BHI target=WORD;
bls: BLS target=WORD;
bge: BGE target=WORD;
blt: BLT target=WORD;
bgt: BGT target=WORD;
ble: BLE target=WORD;

ldr: ldr_pc | ldr_offset | ldrh_offset | ldrsh_offset | ldrb_offset | ldrsb_offset;
ldr_pc: LDR rt=reg COMMA target=WORD ('+' offset=NUM)?;
ldr_offset: LDR rt=reg COMMA '[' rn=reg (COMMA rm=regimm)? ']';
ldrh_offset: LDRH rt=reg COMMA '[' rn=reg (COMMA rm=regimm)? ']';
ldrsh_offset: LDRSH rt=reg COMMA '[' rn=reg (COMMA rm=regimm)? ']';
ldrb_offset: LDRB rt=reg COMMA '[' rn=reg (COMMA rm=regimm)? ']';
ldrsb_offset: LDRSB rt=reg COMMA '[' rn=reg (COMMA rm=regimm)? ']';
store: str_offset | strh_offset | strb_offset;
str_offset: STR rt=reg COMMA '[' rn=reg (COMMA rm=regimm)? ']';
strh_offset: STRH rt=reg COMMA '[' rn=reg (COMMA rm=regimm)? ']';
strb_offset: STRB rt=reg COMMA '[' rn=reg (COMMA rm=regimm)? ']';
stm: (STM|STMIA) rn=reg '!' COMMA reglist;
ldm: (LDM|LDMIA) rn=reg '!' COMMA reglist;

cmp: CMP rn=reg COMMA rm=regimm;
cmn: CMN rn=reg COMMA rm=regimm;

global_directive: dir_code | syntax | dir_gcc;
section: section_text | section_general;
section_general: '.section' s=(WORD | '.text');
section_text: '.text';

directive: align | data | include | dir_size | dir_file | dir_loc | section;
align: '.align' alignment=NUM COMMA fill=NUM;
dir_type: '.type' name=WORD COMMA t=WORD;
globl: '.globl' name=WORD;

dir_code: '.code' NUM;
dir_gcc: '.gcc2_compiled.:';
dir_size: dir_size_abs | dir_size_rel;
dir_size_abs: '.size' WORD COMMA NUM;
dir_size_rel: '.size' WORD COMMA WORD;
dir_file: '.file' file_id=NUM file_path=STRING;
dir_loc: '.loc' file_id=NUM file_line=NUM file_column=NUM;

data: data1word | data2word | data4word | data1num | data2num | data4num;
data1word: DATA1 const=WORD;
data2word: DATA2 const=WORD;
data4word: DATA4 const=WORD ('+' offset=NUM)?;
data1num: DATA1 const=NUM;
data2num: DATA2 const=NUM;
data4num: DATA4 const=NUM;

include: '.include' STRING;

syntax: '.syntax' s=('divided' | 'unified');

reglist: '{' reg (COMMA reg)* '}';
regimm: reg | imm;
reg: REG;
imm: '#' NUM;

STRING: '"' .*? '"';

PUSH: 'push';
POP: 'pop';
ADD: 'add' | 'adds';
SUB: 'sub' | 'subs';
MUL: 'mul' | 'muls';
RSB: 'rsb' | 'rsbs';
NEG: 'neg';
AND: 'and' | 'ands';
ORR: 'orr' | 'orrs';
EOR: 'eor' | 'eors';
LSL: 'lsl' | 'lsls';
LSR: 'lsr' | 'lsrs';
ASL: 'asl' | 'asls';
ASR: 'asr' | 'asrs';
BIC: 'bic' | 'bics';
MOV: 'mov' | 'movs';
MVN: 'mvn' | 'mvns';
B: 'b';
BL: 'bl';
BX: 'bx';
BEQ: 'beq';
BNE: 'bne';
BCS: 'bcs';
BHS: 'bhs';
BCC: 'bcc';
BLO: 'blo';
BMI: 'bmi';
BPL: 'bpl';
BVS: 'bvs';
BVC: 'bvc';
BHI: 'bhi';
BLS: 'bls';
BGE: 'bge';
BLT: 'blt';
BGT: 'bgt';
BLE: 'ble';
LDR: 'ldr';
LDRH: 'ldrh';
LDRSH: 'ldrsh';
LDRB: 'ldrb';
LDRSB: 'ldrsb';
STR: 'str';
STRH: 'strh';
STRB: 'strb';
STM: 'stm';
STMIA: 'stmia';
LDM: 'ldm';
LDMIA: 'ldmia';
CMP: 'cmp';
CMN: 'cmn';

DATA1: '.1byte' | '.byte';
DATA2: '.2byte' | '.half';
DATA4: '.4byte' | '.word';

COMMA: ',';
REG: ('r' [0-9]) | 'lr' | 'pc' | 'sl' | 'sb' | 'ip' | 'sp';
NUM: '-'? '0x'? [0-9a-fA-F]+;
COMMENT: ('@' .*? NL) -> skip;
WORD: [A-Za-z0-9._-]+;
WS: (' ' | '\t') -> skip;
NL: ('\r' | '\r'?'\n') -> skip;
